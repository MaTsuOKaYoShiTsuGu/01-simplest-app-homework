package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PlusTable {
    private String table;

    PlusTable() {
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= i; j++) {
                sb.append(String.format("%d+%d=%-2d", i, j, i + j));
                if (i == j) {
                    sb.append("\n");
                } else {
                    sb.append(" ");
                }
            }
        }
        table = sb.toString();
    }

    @GetMapping("/api/tables/plus")
    String getTable() {
        return table;
    }

}
