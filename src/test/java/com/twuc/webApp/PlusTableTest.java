package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
class PlusTableTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_plus_table_when_use_get_method() throws Exception {
        PlusTable pt = new PlusTable();
        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/plus"))
                .andExpect(MockMvcResultMatchers.content().string(pt.getTable()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith("text/plain"));
    }
}