package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MultiTable {
    private String table;

    MultiTable() {
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= i; j++) {
                if(j==1){
                    sb.append(String.format("%d*%d=%d", i, j, i * j));
                }
                else{
                    sb.append(String.format("%d*%d=%-2d", i, j, i * j));
                }
                if (i == j) {
                    sb.append("\n");
                } else {
                    sb.append(" ");
                }
            }
        }
        table = sb.toString();
    }

    @GetMapping("/api/tables/multiply")
    String getTable() {
        return table;
    }

}
